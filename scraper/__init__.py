# -*- coding: utf-8 -*-

# Testwork for an unknown employer from diesel.elcat.kg
# Author: Art Li, <job.lit.art at gmail.com>

"""
Simple, primitive Search System Scraper
"""

from .search_sistem_scraper import GoogleScraper, YandexScraper
from .sss_exceptions import SSSNotFoundError
from user_agent import UserAgent

_SEARCH_SYSTEMS_SCRAPER = {
    'Google': GoogleScraper,
    'Yandex': YandexScraper,
}


def run(query, ss):
    """
    Demonstration of usage.

    Args:
        - query - Str. query-string
        - ss - Str. search system name

    Returns:
        Prints `title` and `url` for first found page in search result
    """

    try:
        # Select scraper
        _Scraper = _SEARCH_SYSTEMS_SCRAPER[ss]
    except KeyError:
        raise SSSNotFoundError(
            "There is no a search system scraper for [%s]" % ss)

    scraper = _Scraper(user_agent=UserAgent)

    scraper.new_query(query=query)
    entries = scraper.get_search_result_details()

    if entries:
        first_found_page = entries[0]
        print u'%s - %s' % (
            first_found_page.get('title'),
            first_found_page.get('url'))
    else:
        print "Nothing found with `%s` in `%s`" % (query, ss)
