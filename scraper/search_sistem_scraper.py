# -*- coding: utf-8 -*-

# Testwork for an unknown employer from diesel.elcat.kg
# Author: Art Li, <job.lit.art at gmail.com>

import re
from abc import ABCMeta, abstractmethod
from bs4 import BeautifulSoup

from .sss_exceptions import EmptySource, FetchFailed
from user_agent import UserAgent


class ISSScraper:
    """
    Interface for "Search System Scrapers".
    """

    __metaclass__ = ABCMeta

    @abstractmethod
    def new_query():
        """
        Should make queries to the search system.
        """

    @abstractmethod
    def get_search_result_details():
        """
        Should return list of dicts. Each of dict, should represent
        one scraped(pure text) search result item.
        """


class AbsBaseSSScraper(ISSScraper, object):
    """
    Abstract Base Search System Scraper.
    Implements <ISSScraper>.
    """

    __metaclass__ = ABCMeta

    @abstractmethod
    def get_search_result_items(self):
        """
        Should return list of items in search result.
        """

    @abstractmethod
    def get_item_title(self):
        """
        Should return title of the search result item as pure text.
        """

    @abstractmethod
    def get_item_url(self):
        """
        Should return url of the search result item as pure text.
        """

    def __init__(self, *args, **kwargs):
        """
        Args:
            - kwargs.query - Str. (Default: '')
            - kwargs.user_agent - <IUserAgent>. (Default: testwork1.UserAgent)
        """
        self.query = kwargs.get('query', '')

        ua = kwargs.get('user_agent', UserAgent)
        self.ua = ua()

        self._dom = None

    @property
    def base_url(self):
        return self._URL

    def get_url(self):
        """
        Prepare filan queries URL
        """
        return u'%s%s' % (self.base_url, self.query)

    @property
    def dom(self):
        """
        Returns:
            BeautifulSoup instanse
        """
        return self._dom

    @dom.setter
    def dom(self, dom):
        """
        Init BeautifulSoup DOM.

        Args:
            dom - raw HTML / XML
        """
        self._dom = BeautifulSoup(dom)

    def new_query(self, **kwargs):
        """
        Look at <ISSScraper>

        Args:
            - kwargs.query - Str.
        """
        self.query = kwargs.get('query')
        self.dom = self._fetch_source()

    def get_search_result_details(self):
        """
        Look at <ISSScraper>
        """

        search_result_items = self.get_search_result_items()
        if not search_result_items:
            return None

        search_result_details = []

        for item in search_result_items:
            details = self._get_item_datails(item)
            search_result_details.append(details)

        return search_result_details

    def _get_item_datails(self, item):
        """
        Args:
            - item - BeautifulSoup instanse

        Returns:
            dict{'title': value, 'url': value}
                value - is pure text
        """

        details = {
            'title': self.get_item_title(item),
            'url': self.get_item_url(item),
            }

        return details

    def _fetch_source(self):
        """
        Delegates source fetching.
        """
        try:
            source = self.ua.fetch_source(self.get_url())
        except Exception as e:
            raise FetchFailed((
                'Fetching is failed. URL: %s'
                '\nError: %s') % (self.get_url(), e))

        if not source:
            raise EmptySource(
                "Empty source has been received. URL: %s" % self.get_url())

        return source

    @staticmethod
    def _extract_text(entry):
        """
        Returns pure text.

        Args:
            - entry - BeautifulSoup instanse
        """
        return entry.get_text()


class GoogleScraper(AbsBaseSSScraper):
    """
    Google Search System Scraper.
    Extends <AbsBaseSSScraper>.
    """

    _URL = "https://www.google.ru/search?q="

    def get_search_result_items(self):
        return self.dom.select('#search li.g')

    def get_item_title(self, item):
        try:
            title = item.select('h3.r a')[0]
        except:
            return None

        return self._extract_text(title)

    def get_item_url(self, item):
        try:
            url = item.select('h3.r a')[0].get('href')
        except:
            return None

        return self._extract_url(url)

    @staticmethod
    def _extract_url(entry):
        """
        Returns pure URL

        Args:
            - entry - Str.
        """

        regex = re.compile(r'\/url\?q\=(.*?)\&.*', re.UNICODE)
        regres = regex.match(entry)

        if regres:
            return regres.group(1)
        return None


class YandexScraper(AbsBaseSSScraper):
    """
    Yandex Search System Scraper.
    Extends <AbsBaseSSScraper>.
    """

    _URL = "http://yandex.ru/yandsearch?text="

    def get_search_result_items(self):
        try:
            general_rearch_result = self.dom.select('div.serp-list')[0]
        except:
            return None

        return general_rearch_result.select('div.serp-item')

    def get_item_title(self, item):
        try:
            title = item.select('h2 a')[0]
        except:
            return None

        return self._extract_text(title)

    def get_item_url(self, item):
        try:
            url = item.select('h2 a')[0].get('href')
        except:
            return None

        return url
