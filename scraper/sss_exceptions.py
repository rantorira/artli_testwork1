# -*- coding: utf-8 -*-

# Testwork for an unknown employer from diesel.elcat.kg
# Author: Art Li, <job.lit.art at gmail.com>


class SSSNotFoundError(Exception):
    """
    Exception raised when a search system scraper is not found.
    """
    pass


class EmptySource(Exception):
    """
    Exception raised when the scraper has received an empty source.
    """
    pass


class FetchFailed(Exception):
    """
    Exception raised when the scraper can't fetch source.
    """
    pass
