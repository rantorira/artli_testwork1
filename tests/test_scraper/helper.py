#! /usr/bin/env python
# -*- coding: utf-8 -*-

# Testwork for an unknown employer from diesel.elcat.kg
# Author: Art Li, <job.lit.art at gmail.com>

import os


class Helper(object):
    """
    Help to organize tests
    """

    _base_path = os.path.realpath(__file__)
    _fixture_dir = os.path.join(
        os.path.dirname(os.path.dirname(_base_path)),
        'fixtures')

    def __init__(self, *args, **kwargs):
        """
        Args:
            - kwargs.fixture_name - Fixture identifier
        """
        self._fixture_path = os.path.join(
            self._fixture_dir,
            kwargs.get('fixture_name', ''))

    def load_fixtures(self):
        """
        Load fixtures from file
        """
        with open(self._fixture_path) as file_handle:
            html = file_handle.read()

        return html
