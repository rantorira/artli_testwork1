#! /usr/bin/env python
# coding: spec
# -*- coding: utf-8 -*-

# Testwork for an unknown employer from diesel.elcat.kg
# Author: Art Li, <job.lit.art at gmail.com>

import nose
from expects import *
from mock import Mock
from noseOfYeti.tokeniser.support import noy_sup_setUp

from helper import Helper
from scraper.search_sistem_scraper import GoogleScraper

Scraper = GoogleScraper

describe 'GoogleScraper':
    before_each:
        helper = Helper(fixture_name='google')
        mock = Mock()
        instance = mock.return_value
        instance.fetch_source.return_value = helper.load_fixtures()

        self.scraper = Scraper(user_agent=mock)

    it "should have class variable with base URL":
        expect(self.scraper._URL).to(be_a(basestring))

    describe "get_search_result_items":
        it "should return not empty list":
            self.scraper.new_query()

            result = self.scraper.get_search_result_items()

            expect(result).to(be_a(list))
            expect(result).not_to(be_empty)
