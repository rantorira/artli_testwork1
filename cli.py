#! /usr/bin/env python
# -*- coding: utf-8 -*-

# Testwork for an unknown employer from diesel.elcat.kg
# Author: Art Li, <job.lit.art at gmail.com>

from argparse import ArgumentParser
from scraper import run


def parse_args():
    """
    CLI for testwork1

    Args:
        -q - Srt.
        -ss - Str.

    Returns: <`Title`> - <`URL`>
        - `Title` - The title of first fount page
        - `URL` - Link to found page
    """

    parser = ArgumentParser(
        description="""Returns the title and URL of the first result for the entered
                       query from a specified search system""",)

    # -q
    parser.add_argument(
        '-q', '--query', dest='query', default='qwe', type=str,
        help='The query, that you wish to find. (Default: qwe)',
        metavar='',
        )

    # -ss
    search_sistems = ['Google', 'Yandex']
    parser.add_argument(
        '-ss', '--search_sistem', dest='ss', default='Google', type=str,
        help="""The search sistem in which you want to search.
                (Default: Google)""",
        nargs='?', choices=search_sistems,
        )

    args = parser.parse_args()

    # Validation:
    if not args.query or not args.ss:
        parser.print_help()
        exception = "\nExecution error: Bad arguments\n"

        parser.exit(1, exception)

    # Run:
    try:
        run(args.query, args.ss)
    except Exception as e:
        parser.print_help()
        exception = "\nExecution error: %s.\n" % e

        print(exception)
        raise

# Process:
if __name__ == '__main__':
    parse_args()
