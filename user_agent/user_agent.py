# -*- coding: utf-8 -*-

# Testwork for an unknown employer from diesel.elcat.kg
# Author: Art Li, <job.lit.art at gmail.com>

from abc import ABCMeta, abstractmethod
from splinter import Browser as BrowserFactory


class IUserAgent():
    """
    Interface for any useragents, which usable in this project
    """

    __metaclass__ = ABCMeta

    @abstractmethod
    def fetch_source():
        """ Should return a raw HTML """


class UserAgent(IUserAgent):
    """
    The useragent, based on the "splinter test framework".
    It is a simple object, only for a Testwork.
    And it is less flexible than could be.
    """

    def __init__(self, **kwargs):
        self.browser = BrowserFactory(driver_name="phantomjs")

    def fetch_source(self, url):
        """
        Returns HTML

        Args:
            - url - Str. Assumme http based URI.
        """
        with self.browser as browser:
            browser.visit(url)
            html = browser.html

        return html
